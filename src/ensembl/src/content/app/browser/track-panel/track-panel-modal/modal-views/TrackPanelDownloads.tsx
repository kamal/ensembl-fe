import React from 'react';

const TrackPanelDownloads = () => {
  return (
    <section className="trackPanelDownloads">
      <h3>Downloads</h3>
      <p>Export your browser configurations as images or data</p>
      <p>Not ready yet &hellip;</p>
    </section>
  );
};

export default TrackPanelDownloads;
