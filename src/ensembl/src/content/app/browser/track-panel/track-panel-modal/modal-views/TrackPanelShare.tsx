import React from 'react';

const TrackPanelShare = () => {
  return (
    <section className="trackPanelShare">
      <h3>Share</h3>
      <p>Share your browser configurations or your own data views</p>
      <p>Not ready yet &hellip;</p>
    </section>
  );
};

export default TrackPanelShare;
