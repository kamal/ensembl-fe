import Genes from './genes/Genes';
import Proteins from './proteins/Proteins';

export { Genes, Proteins };
