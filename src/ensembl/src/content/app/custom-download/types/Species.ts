type Species = {
  name: string;
  assembly: string;
  display_name: string;
};

export default Species;
