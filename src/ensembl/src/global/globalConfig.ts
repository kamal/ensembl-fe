export const assetsUrl = '/static';
export const imgBaseUrl = `${assetsUrl}/img`;

export enum BreakpointWidth {
  PHONE = 0,
  TABLET = 600,
  LAPTOP = 900,
  DESKTOP = 1400,
  BIG_DESKTOP = 1800
}

export enum AppName {
  GENOME_BROWSER = 'Genome browser',
  SPECIES_SELECTOR = 'Species selector',
  CUSTOM_DOWNLOADS = 'Custom downloads'
}
