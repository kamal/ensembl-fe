export default {
  name: 'ensembl_privacy_policy',
  version: '2.0.0',
  policyUrl: 'https://www.ebi.ac.uk/data-protection/ensembl/privacy-notice',
  termsUrl: 'https://www.ebi.ac.uk/about/terms-of-use'
};
